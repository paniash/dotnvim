" Language: gitattributes
" Maintainer: Ashish Panigrahi <ashish.panigrahi@protonmail.com>
" Latest Revision: 30 August 2021

if exists("b:current_syntax")
    finish
endif

syn keyword gitSection linguist-detectable linguist-documentation
syn keyword gitSection linguist-generated linguist-vendored
syn keyword gitFiletype text binary
syn match gitExtension '\*'

hi def link gitSection  Keyword
hi def link gitFiletype Type
hi def link gitExtension Constant


let b:current_syntax = "gitattributes"
